# Whisper Transcription

Transcribe a media file with *OpenAI Whisper*.


## Setup

### Get Repository

```bash
cd /your/target/dir
git clone https://git.fairkom.net/emb/displ.eu/transposer/adapters/whisper-transcription.git
cd whisper-transcription
```


### Dependencies

An *Adapter* always runs within the environment of the *Service Connector*. For that reason dependencies have to be installed into the connector's environment.

To run the models either CPU or GPU can be utilized. Please consult [tensorflow.org/install/pip](https://www.tensorflow.org/install/pip) for guidance concerning both, hardware and software, system requirements and the tools needed.

To use *TesorFlow* and *PyTorch*, Python >=3.9 is required. Ensure you have compatible versions installed, or use [`pyenv`](https://github.com/pyenv/pyenv) for Python version management.


#### Install

We use *Anaconda* for this implementation, because it provides better support for scientific applications.

See `INSTALL_<INSTANCE TYPE>.md` to find out how to install *Anaconda* and its dependencies.


* Add `pytorch` channel
```bash
conda config --add channels pytorch
```

* (Re)Create environment<br />
It showed, that the dependencies of this adapter cause the `conda` dependency solver to choke. For this reason, it is advisable to merge the dependencies of the connector with the ones of this adapter and (re)create the environment.
```bash
# If the environment already exists
conda remove -n <environment name> --all
# Merge requirements. <mode>: cpu | gpu
cat /path/to/connector/requirements.conda.txt > requirements.txt && cat requirements.conda.<mode>.txt >> requirements.txt
# Create environment with dependencies
conda create -n <environment-name> --file requirements.txt
```

* Activate the *Connector's* environment
```bash
conda activate -n <environment name>
```

* Install additional requirements<br />
If this installation uses a GPU, use CUDA enabled versions.
```bash
# Reference: https://pytorch.org/get-started/locally/
# When using vanilla Python (only version 11.8 with explicit GPU support available)
pip install torch torchaudio --index-url https://download.pytorch.org/whl/cu118
# When using 'conda'
conda install pytorch torchaudio pytorch-cuda=12.1 -c pytorch -c nvidia
```
If this installation has no GPU support, install the CPU versions of `torch` and `torchaudio`.
```bash
conda install -n <environment name> --file requirements.cpu.txt
pip install \
    torch==2.1.0+cpu \
    torchaudio==2.1.0+cpu \
    -f https://download.pytorch.org/whl/torch_stable.html
```

* Install OpenAI-Whisper
```bash
pip install openai-whisper
```

* Install `whisper-timestamped`
```bash
pip install git+https://github.com/linto-ai/whisper-timestamped
```



### Run Application

To run the application, start the connector that uses this adapter.

Visit the connector's README.md for information concerning installation and running.


