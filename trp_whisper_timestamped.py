#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ========================================================================================
# 
# Copyright 2023 Kanishk Raj Jaiswal @kanishk.raj.jaiswal
#                Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================
# Whisper Timestamped

import os
import sys
import types
import typing
import logging

import re
import json

import numpy as np
import torch
import whisper_timestamped

from dotenv import load_dotenv

import trp_config


BIN_DIR = os.path.dirname(os.path.realpath(__file__))


# MODELS = {
#     # 'multi': {
#     #     'tiny': 'tiny',
#     #     'base': 'base',
#     #     'small': 'small',
#     #     'medium': 'medium',
#     #     'large': 'large-v2'
#     # },
#     'multi': {
#         'tiny': 'openai/whisper-tiny',
#         'base': 'openai/whisper-base',
#         'small': 'openai/whisper-small',
#         'medium': 'openai/whisper-medium',
#         #'large': 'openai/whisper-large',
#         'large': 'openai/whisper-large-v2'
#     },
#     'en': {
#         'tiny': 'openai/whisper-tiny.en',
#         'base': 'openai/whisper-base.en',
#         'small': 'openai/whisper-small.en',
#         'medium': 'openai/whisper-medium.en',
#         #'large': 'openai/whisper-large',
#         'large': 'openai/whisper-large-v2'
#     }
# }


# MODEL_LANG = 'multi'
# MODEL_TYPE = 'large'



class TrpWhisperTimestamped:
    __slots__ = [
        'config',
        'has_cuda',
        'device',
        'model',
        'models',
        'transcribe_opts'
    ]
    
    def __init__(self, config: str | trp_config.TrpConfig | None = None):
        logging.debug('TrpWhisperTimestamped')
        
        self.config = None
        config_files = []
        if isinstance(config, trp_config.TrpConfig):
            self.config = config
        elif isinstance(config, str):
            if os.path.isfile(config):
                config_files.append(config)
        
        if self.config is None:
            self.config = trp_config.TrpConfig({
                'defaults': {
                    'dir': BIN_DIR,
                    'file': 'defaults.ini',
                    'data': {
                        'general': {
                            'lang': 'multi',
                            'size': 'large'
                        },
                        'models': {}
                    }
                },
                'files': config_files
            }, True, 'TRP_WHISPER')
        
        if self.config is None:
            raise Exception(f'No config found')
        
        
        print('config:', self.config)
        print('config.sections:', self.config.sections)
        print('config.get(general):', self.config.get('general'))
        print('config.get(general).options:', self.config.get('general').options)
        print('config.get(models):', self.config.get('models'))
        print('config.get(models).options:', self.config.get('models').options)
        
        
        self.has_cuda = False
        try:
            torch.cuda.init()
            self.has_cuda = torch.cuda.is_available()
        except:
            pass
        
        if self.has_cuda:
            self.device = torch.device("cuda")
        else:
            self.device = torch.device("cpu")
        logging.info(f'TrpWhisperTimestamped - Device: {self.device}')
        
        self.models = self.config.get('models').get('multi', {
            'tiny': 'openai/whisper-tiny',
            'base': 'openai/whisper-base',
            'small': 'openai/whisper-small',
            'medium': 'openai/whisper-medium',
            'large': 'openai/whisper-large-v2'
        })
        self.model_lang = self.config.get('general').get('lang', 'multi')
        self.model_size = self.config.get('general').get('size', 'large')
        
        self.model_name = 'openai/whisper-large-v2'
        if (
            self.model_lang in self.models and
            self.model_size in self.models[self.model_lang]
        ):
            self.model_name = self.models[self.model_lang][self.model_size]
        
        self.transcribe_opts = {}
        if self.config.get('general').has('beam_size'):
            self.transcribe_opts['beam_size'] = self.config.get('general').get('beam_size')
        if self.config.get('general').has('best_of'):
            self.transcribe_opts['best_of'] = self.config.get('general').get('best_of')
        if self.config.get('general').has('temperature'):
            self.transcribe_opts['temperature'] = self.config.get('general').get('temperature')
        
        logging.info(f'Loading and initializing model: {MODELS[MODEL_LANG][MODEL_TYPE]}')
        self.model = whisper_timestamped.load_model(MODELS[MODEL_LANG][MODEL_TYPE], device=self.device)
        logging.info(f'Model load and init done')
        
        
        
        #self.models[self.model_lang]
        
        
        
        print('self.has_cuda:', self.has_cuda)
        print('self.device:', self.device)
        print('self.model:', self.model)
    
    
    
    def transcibe(self, audio: str | np.float32 | None = None, lang: str | None = None) -> dict | None:
        logging.debug('transcibe')
        
        t_opts = self.transcribe_opts.copy()
        if lang is not None:
            t_opts['language'] = lang
        
        print('transcibe ... t_opts:', t_opts)



