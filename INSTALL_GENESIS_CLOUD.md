# Installation on [Genesis Cloud](https://www.genesiscloud.com/)

[Genesis Cloud](https://www.genesiscloud.com/)

Described here is the installation of *Whisper* on an Instance in the *Genesis Cloud*.

The steps here are tested on an instance using:
* Hardware
  * 4 CPU x86/amd64
  * NVIDIA RTX 3090
* Software
  * Ubuntu 22.04
  * NVIDIA GPU Driver 535



## Setup Instance

Upgrade system

```bash
apt update
apt upgrade
```

It is very likely that a reboot is required.

```bash
reboot
```

### Install Tools

#### Base Packages:

```bash
apt install \
apt-transport-https \
debian-keyring \
build-essential software-properties-common devscripts checkinstall \
cmake clang autotools-dev automake autogen autoconf libtool pkg-config \
flex bison gdb \
git mercurial \
yasm gettext \
libssl-dev libreadline-dev ssl-cert \
python3-full python3-dev python3-pip python3-virtualenv python3-venv python3-gi python-gi-dev \
vim screen net-tools locate unzip
```


#### Libraries to compile Python

```bash
apt install libbz2-dev lzma-dev liblz-dev liblzma-dev sqlite3 libsqlite3-dev
```


#### Keyring

Make sure we have a keyring dir.

```bash
mkdir -p /etc/apt/keyrings
```


#### Conda

Consult [Installing on Linux](https://docs.conda.io/projects/conda/en/stable/user-guide/install/linux.html) to install *Conda* on the instance.


##### Install (short)

```bash
# We assume to be root here

cd ~/
wget https://repo.anaconda.com/archive/Anaconda3-2023.09-0-Linux-x86_64.sh
bash Anaconda3-2023.09-0-Linux-x86_64.sh
# Accept license
# Install prefix: `/var/lib/anaconda3`
# Add to .bashrc
```

Copy `conda` setup to `.bashrc` of unprivileged target user
```bash
# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/var/lib/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
  eval "$__conda_setup"
else
  if [ -f "/var/lib/anaconda3/etc/profile.d/conda.sh" ]; then
    . "/var/lib/anaconda3/etc/profile.d/conda.sh"
  else
    export PATH="/var/lib/anaconda3/bin:$PATH"
  fi
fi
unset __conda_setup
# <<< conda initialize <<<
```

Resolving environments in conda can take a very long time.<br />
As of Nov '23 a new resolver lib should become the standard.<br />
See https://www.anaconda.com/blog/a-faster-conda-for-a-growing-community<br />
If you encounter (sometimes) hour-long 'Solving environment' prompts, install the lib manually.
```bash
conda update -n base conda
conda install -n base conda-libmamba-solver
conda config --set solver libmamba
```


#### AutoSSH

To create stable SSH tunnels, use `autossh`

```bash
apt install autossh
```


#### ffmpeg

```bash
apt install ffmpeg
```



## Setup Application

The application consists of three parts
* The *Service Connector* providing the environment and runtime
* The *Adapter* providing the logic for the process
* The *tunnel* needed to communicate with the *Transposer Pipeline*

To run *Service Connector* we need, besides its source:
* Adapters
* Configs
  * Connector Configs
  * Adapter Configs
* systemd Services


### Service Connector

Clone the *Service Connector* into the desired directory an rename its directory to your liking.

```bash
# Clone the shared libraries
git clone https://git.fairkom.net/emb/displ.eu/transposer/shared-lib.git
# Clone the shared components needed into 'shared-lib'

# Clone the Service Connector
git clone https://git.fairkom.net/emb/displ.eu/transposer/service-connector.git

# rename 'service-connector' to your liking. E.g.: 'whisper-transcription'
mv service-connector whisper-transcription
```

Change into the connector's directory, setup the `venv`, activate it and install the connector's dependencies.

```bash
conda env create -n whisper-transcription -f /path/to/requirements.txt -p /path/to/whisper-transcription

python3 -m venv ./
source bin/activate
pip install -r requirements.txt
```


### Adapters

Create a directory outside the *Service Connector* which will contain your *Adapters*.

By default *Service Connector* will look in the following directories and their direct sub-directories for adapters:
* &lt;connector&gt;/adapters
* /var/lib/transposer/adapters
* ~/.local/lib/transposer/adapters

To use a custom search path, add it to `[adapter] > search_paths` in the connector's config.


E.g. install into user's home:
```bash
mkdir -p ~/.local/lib/transposer/adapters
cd ~/.local/lib/transposer/adapters
# Because this will be an instance that uses Whisper to transcribe media,
# clone the Whisper Transcription adapter
git clone https://git.fairkom.net/emb/displ.eu/transposer/adapters/whisper-transcription.git
```

**Adapters normally contain:**
* module.py (e.g.: whisper-transcription.py)<br/>
The Adapter module
* module.defaults.ini (e.g.: whisper-transcription.defaults.ini)<br/>
Configuration defaults defined by the developer (normally to be used as-is)<br/>
This file contains all available parameters, with their description and default values
* module.ini (e.g.: whisper-transcription.ini)<br/>
The custom configuration file
* README.md
* requirements.txt<br/>
The required modules for the adapter to work.<br/>
This file can be empty or missing, if no additional dependencies exist
* Any number of custom modules and packages


**Install Dependencies**

Make sure you are still in the connector's active environment.

If the adapter contains `requirements.txt` install the dependencies with
```bash
pip install -r requirements.txt
```

For use with the CPU
```bash
# Vanilla Python
pip install -r requirements.cpu.txt

# Anaconda
conda install -n <target environment name> -f requirements.cpu.txt
```

For use with CPU and GPU
```bash
# Vanilla Python
pip install -r requirements.gpu.txt

# Anaconda
conda install -n <target environment name> -f requirements.gpu.txt
```

If the README.md lists dependencies, follow the instructions.


### Config

Although the configs at their default locations ( &lt;connector&gt;/etc/config.ini and &lt;adapter&gt;/&lt;adapter&gt;.ini ) will be picked up by the *Service Connector*, modifying them there will make maintenance unnecessarily hard.

*Service Connector* will also look for connector configs in
* /etc/transposer/connector
* ~/.config/transposer/connector
* /a/custom/path

For adapter configs in
* /etc/transposer/adapters
* ~/.config/transposer/adapters
* /a/custom/path

Additionally to config files, you can change config parameters via environment variables. Environment variables will take precedence. A `.env` file, placed in the connector's directory will be loaded and its variables made available. For the syntax of the variables' names, please consult the `defaults.ini` of the connector.

To set a custom config path, as well as a custom config name, use environment variables.


##### Services

*Service Connector* provides service templates for
* Service Connector
* AutoSSH

To run these, copy the templates to `/etc/systemd/system/` and modify them to fit your setup. You find descriptions inside the `.service` files.

When done, reload *systemd* and start and enable the services.

```bash
sudo cp <connector>/templates/service/service-connector.service /etc/systemd/system/
# If this runs on a remote machine, we also need the tunnel service
sudo cp <connector>/templates/service/transposer-tunnel.service /etc/systemd/system/

# -> modify services in /etc/systemd/system/

sudo systemctl daemon-reload

# the following only if on remote machine
sudo systemctl start transposer-tunnel.service
sudo systemctl enable transposer-tunnel.service

# connector service (always)
sudo systemctl start service-connector.service
sudo systemctl enable service-connector.service
```


