#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ========================================================================================
# 
# Copyright 2023 Kanishk Raj Jaiswal @kanishk.raj.jaiswal
#                Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================
# Whisper Timestamped

import os
import sys
import types
import typing
import logging
import importlib


PROJECT_DIR	         = '/home/ubuntu/connectors/whisper-transcription'


BIN_DIR		         = os.path.dirname(os.path.realpath(__file__))
CONFIG_DIR	         = os.path.realpath(os.path.join(PROJECT_DIR, 'etc'))
MODULES_DIR	         = os.path.realpath(os.path.join(PROJECT_DIR, 'modules'))
ADAPTERS_DIR	     = os.path.realpath(os.path.join(PROJECT_DIR, 'adapters'))
ADAPTERS_CONFIG_DIR	 = os.path.realpath(os.path.join(CONFIG_DIR, 'adapters'))
LOG_DIR		         = os.path.realpath(os.path.join(PROJECT_DIR, 'var/log'))
WORK_DIR	         = os.path.realpath(os.path.join(PROJECT_DIR, 'var/run'))
TMP_DIR	             = os.path.realpath(os.path.join(PROJECT_DIR, 'var/tmp'))

SHARED_LIB_DIR   = os.path.realpath(os.path.join(PROJECT_DIR, '../shared-lib'))
SHARED_MODULES_DIR   = os.path.realpath(os.path.join(SHARED_LIB_DIR, 'modules'))
SHARED_ADAPTERS_DIR  = os.path.realpath(os.path.join(SHARED_LIB_DIR, 'adapters'))

CUSTOM_ADAPTERS_DIRS = []

# Add sub directories of a given path to the search path.
# This is used to add sub directories of the adapters directory.
# This is needed to allow adapters to import modules from their
# own sub directories.
# Only directories that are directly inside 'path and do NOT contain
# an __init__.py file (are themselves not packages) are added.
def add_sub_search_dirs(path: str) -> None:
    for root, dirs, files in os.walk(path):
        for d in dirs:
            if d.startswith('.'):
                continue
            if d.startswith('_'):
                continue
            s_path = os.path.join(root, d)
            for s_root, s_dirs, s_files in os.walk(s_path):
                if '__init__.py' in s_files:
                    break
                sys.path.insert(0, s_path)
                CUSTOM_ADAPTERS_DIRS.append(s_path)
                break
        break

sys.path.insert(0, MODULES_DIR)
sys.path.insert(0, ADAPTERS_DIR)
CUSTOM_ADAPTERS_DIRS.append(ADAPTERS_DIR)
add_sub_search_dirs(ADAPTERS_DIR)
if os.path.isdir(SHARED_MODULES_DIR):
    sys.path.insert(0, SHARED_MODULES_DIR)
    add_sub_search_dirs(SHARED_MODULES_DIR)
if os.path.isdir(SHARED_ADAPTERS_DIR):
    sys.path.insert(0, SHARED_ADAPTERS_DIR)
    CUSTOM_ADAPTERS_DIRS.append(SHARED_ADAPTERS_DIR)
    add_sub_search_dirs(SHARED_ADAPTERS_DIR)

if os.path.isdir('/var/lib/transposer/modules'):
    sys.path.insert(0, '/var/lib/transposer/modules')
if os.path.isdir('/var/lib/transposer/adapters'):
    sys.path.insert(0, '/var/lib/transposer/adapters')
    CUSTOM_ADAPTERS_DIRS.append('/var/lib/transposer/adapters')
    add_sub_search_dirs('/var/lib/transposer/adapters')
        
user_lib_path = os.path.expanduser('~/.local/lib/transposer/modules')
if os.path.isdir(user_lib_path):
    sys.path.insert(0, user_lib_path)
user_lib_path = os.path.expanduser('~/.local/lib/transposer/adapters')
if os.path.isdir(user_lib_path):
    sys.path.insert(0, user_lib_path)
    CUSTOM_ADAPTERS_DIRS.append(user_lib_path)
    add_sub_search_dirs(user_lib_path)



from dotenv import load_dotenv

import trp_config

import trp_whisper_timestamped




trp_whisper = trp_whisper_timestamped.TrpWhisperTimestamped()
    




